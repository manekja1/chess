package com.mycompany.chess;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Honza
 */
public class ChessBoard {

    public Figurine king1 = new Figurine(1, 1);
    public Figurine rook1 = new Figurine(2, 1);
    public Figurine bishop1 = new Figurine(3, 1);
    public Figurine queen1 = new Figurine(4, 1);
    public Figurine knight1 = new Figurine(5, 1);
    public Figurine pawn1 = new Figurine(6, 1);

    public Figurine king2 = new Figurine(1, 2);
    public Figurine rook2 = new Figurine(2, 2);
    public Figurine bishop2 = new Figurine(3, 2);
    public Figurine queen2 = new Figurine(4, 2);
    public Figurine knight2 = new Figurine(5, 2);
    public Figurine pawn2 = new Figurine(6, 2);
    
    private Figurine deadFigurines[];

    private Box[] board = {
        /////A///////////////B/////////////////C/////////////////D////////////////E////////////////F/////////////////G/////////////////H
        /*1*/new Box(rook1), new Box(knight1), new Box(bishop1), new Box(queen1), new Box(king1), new Box(bishop1), new Box(knight1), new Box(rook1),
        /*2*/ new Box(pawn1), new Box(pawn1), new Box(pawn1), /**/ new Box(pawn1), new Box(pawn1), new Box(pawn1), new Box(pawn1), /**/ new Box(pawn1),
        /*3*/ new Box(),/*  */new Box(),/*  */ new Box(),/*    */ new Box(),/*  */ new Box(),/* */ new Box(),/* */ new Box(),/*     */ new Box(),
        /*4*/ new Box(),/*  */new Box(),/*  */ new Box(),/*    */ new Box(),/*  */ new Box(),/* */ new Box(),/* */ new Box(),/*     */ new Box(),
        /*5*/ new Box(),/*  */new Box(),/*  */ new Box(),/*    */ new Box(),/*  */ new Box(),/* */ new Box(),/* */ new Box(),/*     */ new Box(),
        /*6*/ new Box(),/*  */new Box(),/*  */ new Box(),/*    */ new Box(),/*  */ new Box(),/* */ new Box(),/* */ new Box(),/*     */ new Box(),
        /*7*/ new Box(pawn2), new Box(pawn2), new Box(pawn2),/**/ new Box(pawn2), new Box(pawn2), new Box(pawn2), new Box(pawn2), /* */ new Box(pawn2),
        /*8*/ new Box(rook2), new Box(knight2), new Box(bishop2), new Box(queen2), new Box(king2), new Box(bishop2), new Box(knight2), new Box(rook2)

    };

    public ChessBoard() {

    }

    /**
     *
     * @return whole board, read only, writing with move, throwOut
     */
    /*public Box[] getBoard() {
        return board;
    }*/

    /**
     * move figure
     *
     * @param fromX
     * @param fromY
     * @param toX
     * @param toY
     * @return true if move is possible and succesful
     */
    public boolean move(int fromX, int fromY, int toX, int toY) {
        boolean succesful = false;

        return succesful;
    }

    /**
     * same as move but used to throw out another figure
     *
     * @param fromX
     * @param fromY
     * @param toX
     * @param toY
     * @return true if possible and succesful
     */
    public boolean throwOut(int fromX, int fromY, int toX, int toY) {
        boolean succesful = false;

        return succesful;
    }

    /**
     * if pawn pass opposite end of board he can be changed for another figurine
     * that was thrown out before.
     *
     * @param posX position x of pawn
     * @param posY position y of pown
     * @param toFigurine
     * @return true if possible and succesfull
     */
    public boolean changeFigurine(int posX, int posY, Figurine toFigurine) {
        boolean succesful = false;

        return succesful;
    }

}
