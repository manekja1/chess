/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.chess;

/**
 *
 * @author Honza
 */
public class Figurine {
    /**
     * figure IDs:
     * 1-king(kral)
     * 2-rook(vez)
     * 3-bishop(strelec)
     * 4-queen(kralovna)
     * 5-knight(kun)
     * 6-pawn(pesak)     * 
     */
    private int ID;
    private int player;
    
    public Figurine(int ID, int player){
        this.ID=ID;
        this.player=player;        
    }
    
}
